package uz.pdp.footballerservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

//JAMSHID MIRKHADJAYEV 20-May-22  8:29 PM
@NoArgsConstructor
@AllArgsConstructor
@Data
@PackagePrivate
public class AgentDto {
    Integer id;
    String  name;
    Integer countryId;
    Double  balance;
}
