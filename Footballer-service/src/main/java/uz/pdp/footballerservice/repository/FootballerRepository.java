package uz.pdp.footballerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.footballerservice.entity.Footballer;

public interface FootballerRepository extends JpaRepository<Footballer, Integer> {

}
