package uz.pdp.footballerservice.repository;

import org.aspectj.weaver.loadtime.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.footballerservice.entity.Agents;

public interface AgentRepository extends JpaRepository<Agents, Integer> {

}
