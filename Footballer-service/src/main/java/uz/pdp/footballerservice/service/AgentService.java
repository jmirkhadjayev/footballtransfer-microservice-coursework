package uz.pdp.footballerservice.service;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import uz.pdp.footballerservice.common.ApiResponse;
import uz.pdp.footballerservice.entity.Agents;
import uz.pdp.footballerservice.payload.AgentDto;
import uz.pdp.footballerservice.repository.AgentRepository;

import java.util.List;
import java.util.Optional;


//JAMSHID MIRKHADJAYEV 19-May-22  8:14 PM
@Service
@RequiredArgsConstructor
public class AgentService {
    private final AgentRepository agentRepository;


    public ApiResponse getAllAgent() {
        List<Agents> all = agentRepository.findAll();
        if (all.size() == 0) {
            return new ApiResponse("List empty", false);
        }
        return new ApiResponse("Success", true, all);
    }

    public AgentDto getAgentById(Integer id) {
        Optional<Agents> optionalAgent = agentRepository.findById(id);
        if (optionalAgent.isEmpty()) {
            return null;
        }
        Agents agents = optionalAgent.get();
        AgentDto agentDto = new AgentDto();
        agentDto.setBalance(agents.getBalance());
        agentDto.setCountryId(agents.getCountryId());
        agentDto.setName(agents.getName());
        agentDto.setId(agents.getId());
        return agentDto;
    }

    public ApiResponse addAgent(Agents agents) {
        try {
//            Agents save = agentRepository.save(agents);
            Agents save = agentRepository.save(agents);

            return new ApiResponse("Successfully added", true, save);
        } catch (Exception e) {
            return new ApiResponse("Maybe this agent already exist", false);
        }
    }


    public ApiResponse editAgent(Agents agents, Integer id) {
        Optional<Agents> optionalAgent = agentRepository.findById(id);
        if (optionalAgent.isEmpty()) {
            return new ApiResponse("Agents not found", false);
        }
        try {
            Agents editAgents = optionalAgent.get();
            editAgents.setName(agents.getName());
            editAgents.setCountryId(agents.getCountryId());
            editAgents.setBalance(agents.getBalance());
//            editAgents.setStadium(agents.getStadium());
//            editAgents.setDescription(agents.getDescription());
//            editAgents.setName(agents.getName());
//            editAgents.setFrequency(agents.getFrequency());
            Agents save = agentRepository.save(editAgents);
            return new ApiResponse("Successfully edited", true, save);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse deleteAgent(Integer id) {
        try {
            agentRepository.deleteById(id);
            return new ApiResponse("Successfully deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Agent not found", false);
        }
    }



}