package uz.pdp.footballerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.footballerservice.common.ApiResponse;
import uz.pdp.footballerservice.entity.Footballer;
import uz.pdp.footballerservice.payload.FootballerDto;
import uz.pdp.footballerservice.repository.FootballerRepository;

import java.util.List;
import java.util.Optional;

//JAMSHID MIRKHADJAYEV 19-May-22  8:09 PM
@Service
public class FootballService {

    @Autowired
    FootballerRepository footballerRepository;



    public ApiResponse getAllFootballer() {
        List<Footballer> all = footballerRepository.findAll();
        if (all.size() == 0) {
            return new ApiResponse("List empty", false);
        }
        return new ApiResponse("Success", true, all);
    }

    public FootballerDto getFootballerById(Integer id) {
        Optional<Footballer> optionalFootballer = footballerRepository.findById(id);
        if (optionalFootballer.isEmpty()) {
            return null;
        }
        Footballer footballer = optionalFootballer.get();
        FootballerDto footballerDto = new FootballerDto();
        footballerDto.setId(footballer.getId());
        footballerDto.setCurrentFootballClubId(footballer.getCurrentFootballClubId());
        footballerDto.setAge(footballer.getAge());
        footballerDto.setPosition(footballer.getPosition());
        footballerDto.setCountryId(footballer.getCountryId());
        footballerDto.setFullName(footballer.getFullName());
        footballerDto.setTransferValue(footballer.getTransferValue());

        return footballerDto;
    }

    public ApiResponse addFootballer(Footballer footballer) {
        try {
            Footballer save = footballerRepository.save(footballer);
            return new ApiResponse("Successfully added", true, save);
        } catch (Exception e) {
            return new ApiResponse("Maybe this footballer already exist", false);
        }
    }


    public boolean editFootballer(FootballerDto footballer, Integer id) {
        Optional<Footballer> optionalFootballer = footballerRepository.findById(id);
        if (optionalFootballer.isEmpty()) {
            return false;
        }
        try {
            Footballer editFootballer = optionalFootballer.get();
            editFootballer.setFullName(footballer.getFullName());
            editFootballer.setAge(footballer.getAge());
            editFootballer.setPosition(footballer.getPosition());
            editFootballer.setTransferValue(footballer.getTransferValue());
            editFootballer.setCountryId(footballer.getCountryId());
            footballerRepository.save(editFootballer);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ApiResponse deleteFootballer(Integer id) {
        try {
            footballerRepository.deleteById(id);
            return new ApiResponse("Successfully deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Footballer not found", false);
        }
    }



}
