package uz.pdp.footballerservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.util.UUID;

//JAMSHID MIRKHADJAYEV 19-May-22  5:57 PM
@NoArgsConstructor
@AllArgsConstructor
@Data
@PackagePrivate
@Entity(name = "footballer")
public class Footballer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String  fullName;
    Integer age;
    String  position;
    Integer countryId;
    Double  transferValue;
    Integer currentFootballClubId;
}
