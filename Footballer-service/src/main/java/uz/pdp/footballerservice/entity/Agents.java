package uz.pdp.footballerservice.entity;

//JAMSHID MIRKHADJAYEV 19-May-22  6:05 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;

@Data
@NoArgsConstructor
@PackagePrivate
@AllArgsConstructor
@Entity(name = "agents")
public class Agents {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String  name;
    Integer countryId;
    Double  balance;
}
