package uz.pdp.footballerservice.controller;

//JAMSHID MIRKHADJAYEV 19-May-22  8:12 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.footballerservice.common.ApiResponse;
import uz.pdp.footballerservice.entity.Agents;
import uz.pdp.footballerservice.payload.AgentDto;
import uz.pdp.footballerservice.service.AgentService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/football/agent")
public class AgentController {

    private final AgentService agentService;


    @GetMapping
    public HttpEntity<?> getAllAgent() {
        ApiResponse allAgent = agentService.getAllAgent();
        return ResponseEntity.status(allAgent.isSuccess() ? 200 : 400).body(allAgent);
    }

    @GetMapping("/{id}")
    public AgentDto getAgentById(@PathVariable Integer id) {
      return  agentService.getAgentById(id);
    }
    @PutMapping("/{id}")
    public HttpEntity<?> editAgent(@PathVariable Integer id, @RequestBody Agents agents) {
        ApiResponse apiResponse = agentService.editAgent(agents, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

    @PostMapping
    public HttpEntity<?> addAgent(@RequestBody Agents agents) {
        ApiResponse apiResponse = agentService.addAgent(agents);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteAgent(@PathVariable Integer id) {
        ApiResponse apiResponse = agentService.deleteAgent(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }


}
