package uz.pdp.footballerservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.footballerservice.common.ApiResponse;
import uz.pdp.footballerservice.entity.Footballer;
import uz.pdp.footballerservice.payload.FootballerDto;
import uz.pdp.footballerservice.repository.FootballerRepository;
import uz.pdp.footballerservice.service.FootballService;

//JAMSHID MIRKHADJAYEV 19-May-22  7:46 PM
@RestController
@RequiredArgsConstructor
@RequestMapping("api/football/footballer/")
public class FootballerController {

    private final FootballService footballService;
    private final FootballerRepository footballerRepository;


    @GetMapping
    public HttpEntity<?> getAllFootballer() {
        ApiResponse allFootballer = footballService.getAllFootballer();
        return ResponseEntity.status(allFootballer.isSuccess() ? 200 : 400).body(allFootballer);
    }

    @GetMapping("/{id}")
    public FootballerDto getFootballerById(@PathVariable Integer id) {
        return  footballService.getFootballerById(id);
    }

    @GetMapping("/exist/{id}")
    public boolean existFootballerById(@PathVariable Integer id) {
        return footballerRepository.existsById(id);
    }

    @PutMapping("/{id}")
    public boolean editFootballer(@PathVariable Integer id, @RequestBody FootballerDto footballer) {
      return footballService.editFootballer(footballer, id);
    }

    @PostMapping
    public HttpEntity<?> addFootballer(@RequestBody Footballer footballer) {
        ApiResponse apiResponse = footballService.addFootballer(footballer);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteFootballer(@PathVariable Integer id) {
        ApiResponse apiResponse = footballService.deleteFootballer(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }


}
