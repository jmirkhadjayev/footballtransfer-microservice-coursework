package uz.pdp.clubservice.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

    //JAMSHID MIRKHADJAYEV 20-May-22  10:30 AM
@AllArgsConstructor
@NoArgsConstructor
@Data

public class ApiResponse {
    private String message;
    private boolean success;
    private Object object;

    public ApiResponse(String message, boolean success) {
        this.message = message;
        this.success = success;
    }
}




