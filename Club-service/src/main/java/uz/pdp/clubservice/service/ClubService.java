package uz.pdp.clubservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.clubservice.common.ApiResponse;
import uz.pdp.clubservice.entity.Club;
import uz.pdp.clubservice.entity.Country;
import uz.pdp.clubservice.entity.Stadium;
import uz.pdp.clubservice.payload.ClubDto;
import uz.pdp.clubservice.repository.ClubRepository;
import uz.pdp.clubservice.repository.CountryRepository;
import uz.pdp.clubservice.repository.StadiumRepository;

import java.util.List;
import java.util.Optional;

//JAMSHID MIRKHADJAYEV 19-May-22  8:38 PM
@Service
@RequiredArgsConstructor
public class ClubService {

    private final ClubRepository clubRepository;
    private final StadiumRepository stadiumRepository;
    private final CountryRepository countryRepository;


    public ApiResponse getAllClub() {
        List<Club> all = clubRepository.findAll();
        if (all.size() == 0) {
            return new ApiResponse("List empty", false);
        }
        return new ApiResponse("Success", true, all);
    }

    public ClubDto getClubById(Integer id) {
        Optional<Club> optionalClub = clubRepository.findById(id);
        if (optionalClub.isEmpty()) {
      return null;
        }
        Club club = optionalClub.get();
        ClubDto clubDto = new ClubDto();
        clubDto.setName(club.getName());
        clubDto.setDescription(club.getDescription());
        clubDto.setCountryId(club.getCountry().getId());
        clubDto.setBudget(club.getBudget());
        clubDto.setStadiumId(club.getStadium().getId());
        return clubDto;
    }

    public ApiResponse addClub(ClubDto clubDto) {
        try {
            Club club = new Club();
            club.setDescription(clubDto.getDescription());
            club.setName(clubDto.getName());
            Optional<Stadium> optionalStadium = stadiumRepository.findById(clubDto.getStadiumId());
            if (!optionalStadium.isPresent()) {
                return new ApiResponse("Maybe this club already exist", false);
            }
            Stadium stadium = optionalStadium.get();
            club.setStadium(stadium);

            Optional<Country> optionalCountry = countryRepository.findById(clubDto.getCountryId());
            if (!optionalCountry.isPresent()) {
                return new ApiResponse("Maybe this club already exist", false);

            }
            Country country = optionalCountry.get();
            club.setCountry(country);
            Club save = clubRepository.save(club);


            return new ApiResponse("Successfully added", true, save);
        } catch (Exception e) {
            return new ApiResponse("Maybe this cpu already exist", false);
        }
    }


    public ApiResponse editClub(Club club, Integer id) {
        Optional<Club> optionalClub = clubRepository.findById(id);
        if (optionalClub.isEmpty()) {
            return new ApiResponse("Club not found", false);
        }
        try {
            Club editClub = optionalClub.get();
            editClub.setName(club.getName());
            editClub.setCountry(club.getCountry());
            editClub.setStadium(club.getStadium());
            editClub.setDescription(club.getDescription());
//            editClub.setName(club.getName());
//            editClub.setFrequency(club.getFrequency());
            Club save = clubRepository.save(editClub);
            return new ApiResponse("Successfully edited", true, save);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse deleteClub(Integer id) {
        try {
            clubRepository.deleteById(id);
            return new ApiResponse("Successfully deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Cpu not found", false);
        }
    }


}
