package uz.pdp.clubservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.clubservice.common.ApiResponse;
import uz.pdp.clubservice.entity.Stadium;
import uz.pdp.clubservice.entity.Stadium;
import uz.pdp.clubservice.repository.StadiumRepository;

import java.util.List;
import java.util.Optional;

//JAMSHID MIRKHADJAYEV 19-May-22  8:48 PM
@Service
@RequiredArgsConstructor
public class StadiumService {
    private final StadiumRepository stadiumRepository;

    public ApiResponse getAllStadium() {
        List<Stadium> all = stadiumRepository.findAll();
        if (all.size() == 0) {
            return new ApiResponse("List empty", false);
        }
        return new ApiResponse("Success", true, all);
    }

    public ApiResponse getStadiumById(Integer id) {
        Optional<Stadium> optionalStadium = stadiumRepository.findById(id);
        if (optionalStadium.isEmpty()) {
            return new ApiResponse("Stadium not found", false);
        }
        return new ApiResponse("Success", true, optionalStadium);
    }

    public ApiResponse addStadium(Stadium stadium) {
        try {
            Stadium save = stadiumRepository.save(stadium);
            return new ApiResponse("Successfully added", true, save);
        } catch (Exception e) {
            return new ApiResponse("Maybe this Stadium already exist", false);
        }
    }


    public ApiResponse editStadium(Stadium stadium, Integer id) {
        Optional<Stadium> optionalStadium = stadiumRepository.findById(id);
        if (optionalStadium.isEmpty()) {
            return new ApiResponse("Stadium not found", false);
        }
        try {
            Stadium editStadium = optionalStadium.get();
            editStadium.setName(stadium.getName());
            editStadium.setCapacity(stadium.getCapacity());
//            editStadium.setName(stadium.getName());
//            editStadium.setFrequency(stadium.getFrequency());
            Stadium save = stadiumRepository.save(editStadium);
            return new ApiResponse("Successfully edited", true, save);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse deleteStadium(Integer id) {
        try {
            stadiumRepository.deleteById(id);
            return new ApiResponse("Successfully deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Stadium not found", false);
        }
    }
}
