package uz.pdp.clubservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.clubservice.common.ApiResponse;
import uz.pdp.clubservice.entity.Country;
import uz.pdp.clubservice.entity.Country;
import uz.pdp.clubservice.repository.CountryRepository;

import java.util.List;
import java.util.Optional;

//JAMSHID MIRKHADJAYEV 19-May-22  8:44 PM
@Service
@RequiredArgsConstructor
public class CountryService {
    private final CountryRepository countryRepository;

    public ApiResponse getAllCountry() {
        List<Country> all = countryRepository.findAll();
        if (all.size() == 0) {
            return new ApiResponse("List empty", false);
        }
        return new ApiResponse("Success", true, all);
    }

    public ApiResponse getCountryById(Integer id) {
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if (optionalCountry.isEmpty()) {
            return new ApiResponse("Country not found", false);
        }
        return new ApiResponse("Success", true, optionalCountry);
    }

    public ApiResponse addCountry(Country country) {
        try {
            Country save = countryRepository.save(country);
            return new ApiResponse("Successfully added", true, save);
        } catch (Exception e) {
            return new ApiResponse("Maybe this Country already exist", false);
        }
    }


    public ApiResponse editCountry(Country country, Integer id) {
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if (optionalCountry.isEmpty()) {
            return new ApiResponse("Country not found", false);
        }
        try {
            Country editCountry = optionalCountry.get();
            editCountry.setName(country.getName());
//            editCountry.setName(country.getName());
//            editCountry.setFrequency(country.getFrequency());
            Country save = countryRepository.save(editCountry);
            return new ApiResponse("Successfully edited", true, save);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse deleteCountry(Integer id) {
        try {
            countryRepository.deleteById(id);
            return new ApiResponse("Successfully deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Country not found", false);
        }
    }




}
