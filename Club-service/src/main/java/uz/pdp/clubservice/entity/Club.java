package uz.pdp.clubservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;

//JAMSHID MIRKHADJAYEV 19-May-22  6:11 PM
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "clubs")
@PackagePrivate
public class Club {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    @ManyToOne
    Country country;
    String description;

    @OneToOne
    @JoinColumn(name = "stadium_id")
    Stadium stadium;
    Double budget;

}
