package uz.pdp.clubservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//JAMSHID MIRKHADJAYEV 19-May-22  6:14 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "country")
@PackagePrivate
public class Country {
    @Id
    @GeneratedValue
    Integer id;
    String name;

}
