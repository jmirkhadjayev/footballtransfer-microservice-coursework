package uz.pdp.clubservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//JAMSHID MIRKHADJAYEV 19-May-22  6:17 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "station")
@PackagePrivate
public class Stadium {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    Integer capacity;

}
