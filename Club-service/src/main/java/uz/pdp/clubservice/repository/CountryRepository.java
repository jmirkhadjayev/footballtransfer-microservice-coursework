package uz.pdp.clubservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.clubservice.entity.Country;

public interface CountryRepository extends JpaRepository<Country, Integer> {

}
