package uz.pdp.clubservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.clubservice.entity.Stadium;

public interface StadiumRepository extends JpaRepository<Stadium,Integer> {



}
