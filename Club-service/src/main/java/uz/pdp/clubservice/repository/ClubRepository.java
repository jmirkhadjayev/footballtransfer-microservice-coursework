package uz.pdp.clubservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.clubservice.entity.Club;

public interface ClubRepository extends JpaRepository<Club, Integer> {
}
