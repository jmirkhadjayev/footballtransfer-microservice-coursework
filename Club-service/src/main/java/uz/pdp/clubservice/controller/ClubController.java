package uz.pdp.clubservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.clubservice.common.ApiResponse;
import uz.pdp.clubservice.entity.Club;
import uz.pdp.clubservice.payload.ClubDto;
import uz.pdp.clubservice.repository.ClubRepository;
import uz.pdp.clubservice.service.ClubService;

//JAMSHID MIRKHADJAYEV 19-May-22  8:36 PM
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/club-service/club")
public class ClubController {

    @Autowired
    ClubService clubService;
    @Autowired
    ClubRepository clubRepository;


    @GetMapping
    public HttpEntity<?> getAllClub() {
        ApiResponse allClub = clubService.getAllClub();
        return ResponseEntity.status(allClub.isSuccess() ? 200 : 400).body(allClub);
    }

    @GetMapping("/{id}")
    public ClubDto getClubById(@PathVariable Integer id) {
        return clubService.getClubById(id);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editClub(@PathVariable Integer id, @RequestBody Club club) {
        ApiResponse apiResponse = clubService.editClub(club, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

    @PostMapping
    public HttpEntity<?> addClub(@RequestBody ClubDto club) {
        ApiResponse apiResponse = clubService.addClub(club);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteClub(@PathVariable Integer id) {
        ApiResponse apiResponse = clubService.deleteClub(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

    @GetMapping("/exist/{id}")
    public boolean isExistClub(@PathVariable Integer id) {
        return clubRepository.existsById(id);
    }


}
