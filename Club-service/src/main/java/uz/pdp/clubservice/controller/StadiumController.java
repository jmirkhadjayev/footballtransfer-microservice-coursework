package uz.pdp.clubservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.clubservice.common.ApiResponse;
import uz.pdp.clubservice.entity.Club;
import uz.pdp.clubservice.entity.Stadium;
import uz.pdp.clubservice.service.StadiumService;

//JAMSHID MIRKHADJAYEV 19-May-22  8:47 PM
@RestController
@RequestMapping("/api/club-service/stadium")
@RequiredArgsConstructor
public class StadiumController {
    private final StadiumService stadiumService;

    @GetMapping
    public HttpEntity<?> getAllStadium() {
        ApiResponse allStadium = stadiumService.getAllStadium();
        return ResponseEntity.status(allStadium.isSuccess() ? 200 : 400).body(allStadium);
    }
    @GetMapping("/{id}")
    public HttpEntity<?> getStadiumById(@PathVariable Integer id) {
        ApiResponse stadiumById = stadiumService.getStadiumById(id);
        return ResponseEntity.status(stadiumById.isSuccess() ? 200 : 400).body(stadiumById);
    }
    @PutMapping("/{id}")
    public HttpEntity<?> editStadium(@PathVariable Integer id, @RequestBody Stadium stadium) {
        ApiResponse apiResponse = stadiumService.editStadium(stadium, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

    @PostMapping
    public HttpEntity<?> addStadium(@RequestBody Stadium stadium) {
        ApiResponse apiResponse = stadiumService.addStadium(stadium);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteStadium(@PathVariable Integer id) {
        ApiResponse apiResponse = stadiumService.deleteStadium(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

}
