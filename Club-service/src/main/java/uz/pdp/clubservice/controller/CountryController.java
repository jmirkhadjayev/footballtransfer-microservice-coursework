package uz.pdp.clubservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.clubservice.common.ApiResponse;
import uz.pdp.clubservice.entity.Country;
import uz.pdp.clubservice.service.CountryService;

//JAMSHID MIRKHADJAYEV 19-May-22  8:41 PM
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/club-service/country")
public class CountryController {

    private final CountryService countryService;

    @GetMapping
    public HttpEntity<?> getAllCountry() {
        ApiResponse allCountry = countryService.getAllCountry();
        return ResponseEntity.status(allCountry.isSuccess() ? 200 : 400).body(allCountry);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCountryById(@PathVariable Integer id) {
        ApiResponse countryById = countryService.getCountryById(id);
        return ResponseEntity.status(countryById.isSuccess() ? 200 : 400).body(countryById);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editCountry(@PathVariable Integer id, @RequestBody Country country) {
        ApiResponse apiResponse = countryService.editCountry(country, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

    @PostMapping
    public HttpEntity<?> addCountry(@RequestBody Country country) {
        ApiResponse apiResponse = countryService.addCountry(country);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCountry(@PathVariable Integer id) {
        ApiResponse apiResponse = countryService.deleteCountry(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 400).body(apiResponse);
    }

}
