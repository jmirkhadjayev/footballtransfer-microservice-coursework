package uz.pdp.clients.agent;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("footballer-service")
public interface AgentClient {
    @GetMapping("/{id}")
     AgentDto getAgentById(@PathVariable Integer id);
}
