package uz.pdp.clients.football;

import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("football-service")
public interface FootballerClient {
    @GetMapping("/exist/{id}")
    boolean existFootballerById(@PathVariable("id") Integer id);

    @GetMapping("/{id}")
     FootballerDto getFootballerById(@PathVariable ("id")  Integer id);

    @PutMapping("/{id}")
    boolean editFootballer(@PathVariable ("id") Integer  id, @RequestBody FootballerDto footballer);

}
