package uz.pdp.clients.club;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

//JAMSHID MIRKHADJAYEV 20-May-22  5:06 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class ClubDto {
    String name;

    Integer countryId;

    String description;

    Integer stadiumId;
    Double budget;


}
