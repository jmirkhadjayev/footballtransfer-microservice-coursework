package uz.pdp.clients.club;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("club-service")
public interface ClubClient {


    @GetMapping("/exist/{id}")
    boolean isExistClub(@PathVariable("id") Integer id);

 /*   @GetMapping("/hello-world/from/{from}/to/{to}")
    public String getHelloWorld(@PathVariable("from") String from, @PathVariable ("to") String to);*/
    @GetMapping("/{id}")
     ClubDto getClubById(@PathVariable("id") Integer id);
}
