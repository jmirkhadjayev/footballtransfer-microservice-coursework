package uz.pdp.transferservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.transferservice.common.ApiResponse;
import uz.pdp.transferservice.entity.Transfer;
import uz.pdp.transferservice.service.TransferService;

//JAMSHID MIRKHADJAYEV 19-May-22  8:29 PM
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/transfer")
public class TransferController {

    private final TransferService transferService;

 /*   @GetMapping
    public HttpEntity<?> getAllTransfers() {
        ApiResponse allTransfer = transferService.getAllTransfers();
        return ResponseEntity.status(allTransfer.isSuccess() ? 200 : 400).body(allTransfer);
    }*/

  /*  @GetMapping("/{id}")
    public HttpEntity<?> getCpuById(@PathVariable Integer id) {
        ApiResponse cpuById = cpuServise.getCpuById(id);
        return ResponseEntity.status(cpuById.isSuccess() ? 200 : 400).body(cpuById);
    }*/

    @PostMapping
    public HttpEntity<?> addTransfer(@RequestBody Transfer transfer){
        return transferService.addTransfer(transfer);

    }
}
