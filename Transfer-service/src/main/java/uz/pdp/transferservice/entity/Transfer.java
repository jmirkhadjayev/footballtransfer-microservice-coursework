package uz.pdp.transferservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//JAMSHID MIRKHADJAYEV 19-May-22  6:29 PM
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@PackagePrivate
public class Transfer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    Integer footballer_id;
    Integer from_club_id;
    Integer to_club_id;
    Integer come_year;
    Double salary;
}
