package uz.pdp.transferservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.clients.football.FootballerClient;
import uz.pdp.clients.club.ClubClient;
import uz.pdp.clients.football.FootballerDto;
import uz.pdp.transferservice.entity.Transfer;
import uz.pdp.transferservice.repository.TransferRepository;

//JAMSHID MIRKHADJAYEV 19-May-22  8:30 PM
@Service
@RequiredArgsConstructor
public class TransferService {

/*@Autowired
ClubClient clubClient;*/
    private final ClubClient clubClient;
    private final FootballerClient footballerClient;

    private final TransferRepository transferRepository;

    public HttpEntity<?> addTransfer(Transfer transfer) {
        Transfer transferPost = new Transfer();
        boolean existClub = clubClient.isExistClub(transfer.getTo_club_id());
        boolean existClub2 = clubClient.isExistClub(transfer.getFrom_club_id());
        if (!existClub) {
            return ResponseEntity.badRequest().body("Not found");
        }
        if (!existClub2) {
            return ResponseEntity.badRequest().body("Not found");
        }
        boolean existFootballerById = footballerClient.existFootballerById(transfer.getFootballer_id());
        if (!existFootballerById) {
            return ResponseEntity.badRequest().body("Not found");
        }
        transferPost.setTo_club_id(transfer.getTo_club_id());
        transferPost.setFrom_club_id(transfer.getFrom_club_id());
        transferPost.setCome_year(transfer.getCome_year());
        transferPost.setSalary(transfer.getSalary());

        transferPost.setFootballer_id(transfer.getFootballer_id());

        FootballerDto footballer = footballerClient.getFootballerById(transfer.getFootballer_id());
        footballer.setCurrentFootballClubId(transfer.getTo_club_id());
        footballerClient.editFootballer(footballer.getId(),footballer);


        Transfer save = transferRepository.save(transferPost);
        return ResponseEntity.ok(save);

    }
}
