package uz.pdp.transferservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.transferservice.entity.Transfer;

public interface TransferRepository extends JpaRepository<Transfer, Integer> {

}
